import styled from "styled-components";

export const Card = styled.div`
    @import url("https://fonts.googleapis.com/css2?family=Roboto:wght@500;700&display=swap");
    font-family: Roboto;
    font-size: 14px;
    width: 320px;
    height: 500px;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    padding: 10px;
    transition: 0.3s;
    margin-top: 80px;
    background-color: #ffffff;
    border-radius: 2.4px;
    color: #8d8c8c;
    overflow: hidden;
    text-overflow: ellipsis;
    @media(min-width: 500px){
      width: 420px;
      padding:30px;
    }
    @media(min-width: 800px){
      width: 650px;
      padding:50px;
    }
    @media(min-width: 1000px){
      width: 800px;
      padding:50px;

    }
`;

export const CardHeader = styled.div`
    width: 100%;
    height: 30%;
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: #7dc075;
    border-radius: 2.4px;
    margin-bottom: 20px;
    color: #ffffff;
    font-size: 50px;
`;

export const BackIcon = styled.img`
  width: 30px;
  position: absolute;
  left: 20px;
`;

export const Title = styled.p`
    @import url("https://fonts.googleapis.com/css2?family=Roboto:wght@500;700&display=swap");
    font-family: Roboto;
    font-size: 16px;
    color: white;
    align-self: flex-end;
    text-transform: uppercase;
  
`;
