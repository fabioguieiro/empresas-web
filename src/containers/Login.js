import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
import LogoImg from "../assets/imgs/logo-home.png";
import classes from "./Login.module.css";
import Spinner from "../components/Spinner";

import {
  Logo,
  Container,
  Welcome,
  Description,
  PasswordInput,
  MailInput,
  SubmitButton,
} from "./LoginStyles";

function Login() {
  const history = useHistory();
  const [username, setUsername] = useState("");
  const [logged, setLogged] = useState(false);
  const [password, setPassword] = useState("");

  async function LoginHandler() {
    async function loginAPI() {
      setLogged(true);
      try {
        const loginResponse = await axios.post(
          "https://empresas.ioasys.com.br/api/v1/users/auth/sign_in",
          {
            email: username,
            password: password,
          }
        );

        axios.defaults.headers.common["access-token"] = loginResponse.headers["access-token"]
        axios.defaults.headers.common["client"] = loginResponse.headers["client"]
        axios.defaults.headers.common["uid"] = loginResponse.headers["uid"]
        history.push('/enterprises',)
      } catch (error) {
        console.log(error);
        setLogged(false);
      }
    }

    loginAPI();

  }

  return (
    <div className="d-flex justify-content-center align-items-center">

    <Container className={logged ? classes.backdrop : null}>
      <Logo src={LogoImg} />
      <Welcome>BEM VINDO AO EMPRESAS</Welcome>
      <Description>
        Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan
      </Description>
      <MailInput
        type="text"
        placeholder="E-mail"
        required
        onChange={(e) => setUsername(e.target.value)}
        ></MailInput>
      <PasswordInput
        type="password"
        placeholder="Senha"
        required
        onChange={(e) => {setPassword(e.target.value)}}
        ></PasswordInput>
      <SubmitButton type="submit" onClick={LoginHandler} value="ENTRAR" />
    </Container>
      {logged ? <Spinner /> : null}
        </div>
  );
}

export default Login;
