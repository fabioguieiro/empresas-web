import styled from "styled-components";
import SearchIcon from "../assets/svgs/ic-search-copy.svg";

export const Toolbar = styled.div`
  position: fixed;
  height: 56px;
  width: 100%;
  top: 0;
  left: 0;
  background: rgb(238, 80, 119);
  background: linear-gradient(
    180deg,
    rgba(238, 80, 119, 1) 0%,
    rgba(187, 63, 103, 1) 100%
  );
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0 20px;
  box-sizing: border-box;
  z-index: 90;
  nav {
    height: 100%;
  }
`;

export const StyledLogo = styled.img`
  position: relative;
  width: 150px;
`;

export const StyledSearch = styled.img`
  width: 50px;
  position: absolute;
  right: 20px;
`;

export const Tip = styled.p`
  @import url("https://fonts.googleapis.com/css2?family=Roboto:wght@500;700&display=swap");
  font-family: Roboto;
  font-size: 16px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.22;
  letter-spacing: -0.23px;
  text-align: center;
  color: #383743;
  margin-top: 400px;
`;

export const Card = styled.div`
    position: relative;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    padding: 10px;
    transition: 0.3s;
    width: 320px;
    height:50px;
    margin-top: 20px;
    background-color: #ffffff;
    border-radius: 2.4px;
    flex-direction: row !important;
    @media(min-width: 500px){
      width: 420px;
      height: 80px;
    }
    @media(min-width: 800px){
      width: 650px;
      height: 80px;
    }
    @media(min-width: 1000px){
      width: 800px;
      height: 80px;
    }
}
  hover{
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);}
`;

export const Container = styled.div`
  margin-top: 80px;
`;

export const SearchInput = styled.input`
  @import url("https://fonts.googleapis.com/css2?family=Roboto:wght@500;700&display=swap");
  background: transparent;
  background-image: url(${SearchIcon});
  background-repeat: no-repeat;
  background-size: 30px;
  width: 80%;
  font-family: "Roboto", sans-serif;
  font-size: 11px;
  padding: 10px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  border: none;
  border-bottom: 1px solid #ffffff;
  color: #ffffff;
  padding-left: 30px;
  ::placeholder {
    color: #991237;
  }
`;

export const CardHeader = styled.div`
    width: 20%;
    height: 30px;
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: #7dc075;
    border-radius: 2.4px;
    color: #ffffff;
    font-size: 12px;
    margin-right: 10px;
    font-weight: bold;

    @media(min-width: 500px){
      width: 15%;
      height: 60px;
    }

`;
