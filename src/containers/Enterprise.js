import React, { useEffect, useState } from "react";
import { Toolbar } from "./EnterprisesFeedStyles";
import { Card, CardHeader, BackIcon, Title } from "./EnterpriseStyles";
import Back from '../assets/svgs/arrow-left-solid.svg'
import { useHistory, useLocation } from "react-router-dom";

function Enterprise(props) {
  const history = useHistory();
  const location = useLocation();
  const [data, setData] = useState({});
  useEffect(() => {
    function prepareData() {
      setData(location.data.data[`${location.id.EnterpriseId - 1}`]);
    }
    prepareData()
  }, [location, history]);
  function goBackHandler() {
    history.goBack()
  }

  return (
    <div>
      <Toolbar>
        <BackIcon onClick={goBackHandler} src={Back}></BackIcon>
        <Title>{data.enterprise_name ? data.enterprise_name : null}</Title>
      </Toolbar>
      <Card>
        <CardHeader>{data.enterprise_name ? data.enterprise_name[0] : null}</CardHeader>
        {data.description ? <p>{data.description}</p> : null}
      </Card>
    </div>
  );
}

export default Enterprise;
