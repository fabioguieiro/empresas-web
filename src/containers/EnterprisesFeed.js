import React, { useEffect, useState } from "react";
import {
  Toolbar,
  StyledLogo,
  StyledSearch,
  Tip,
  Card,
  Container,
  SearchInput,
  CardHeader
} from "./EnterprisesFeedStyles";

import axios from "axios";
import Logo from "../assets/imgs/logo-nav.png";
import Search from "../assets/svgs/ic-search-copy.svg";
import { useHistory } from "react-router-dom";

function EnterprisesFeed(props) {
  const history = useHistory();
  const [data, setData] = useState("");
  const [search, setSearch] = useState("");
  const [searchClicked, setSearchClicked] = useState(false);
  let rows = [];
  useEffect(() => {
    async function fetchData() {
      try {
        const enterprisesResponse = await axios.get(
          "https://empresas.ioasys.com.br/api/v1/enterprises"
        );
        setData(enterprisesResponse.data.enterprises);
      } catch (error) {
        console.log(error);
      }
    }

    fetchData();
  }, []);
  let filteredEnterprises = [];
  if (data.length > 0) {
    if (search !== "") {
      filteredEnterprises = data.filter((enterprise) => {
        return (
          enterprise.enterprise_name
            .toLowerCase()
            .indexOf(search.toLowerCase()) !== -1
        );
      });
    }
  }
  function prepareDataToShow() {
    if (data.length > 0) {
      filteredEnterprises.map((item) => {
        return (

          rows.push(
            <Card
              key={item.id}
              onClick={() => chooseEnterprise(item.id)}
              role="button"
              className="card d-flex justify-content-start"
            >
              <CardHeader>{item.enterprise_name[0]}</CardHeader>
              {item.enterprise_name}
            </Card>
          )
        );
      });
    }
  }
  prepareDataToShow();
  function switchSearch() {
    setSearchClicked(true);
  }
  function updateSearchHandler(event) {
    setSearch(event.target.value);
  }
  function chooseEnterprise(EnterpriseId) {
    history.push({
      pathname: "/enterprises/" + EnterpriseId,
      data: { data },
      id: { EnterpriseId },
    });
  }

  return (
    <div>
      <Toolbar>
        {searchClicked ? (
          <SearchInput
            placeholder="Pesquisar"
            value={search}
            autoFocus
            onChange={updateSearchHandler.bind(this)}
          />
        ) : (
            <>
              <StyledLogo src={Logo}></StyledLogo>
              <StyledSearch onClick={switchSearch} src={Search}></StyledSearch>
            </>
          )}
      </Toolbar>
      <Container className="container-fluid">
        {rows.length > 0 ? rows : null}
      </Container>
      {searchClicked ? null : <Tip>Clique na busca para iniciar.</Tip>}
    </div>
  );
}

export default EnterprisesFeed;
