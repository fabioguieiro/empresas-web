import styled from 'styled-components'
import mailIcon from '../assets/svgs/ic-email.svg'
import passwordIcon from '../assets/svgs/ic-cadeado.svg'

export const Logo = styled.img`
    width: 180px;
    margin-bottom: 5%; 
`;

export const Container = styled.div`
    width: 40%;
    height: 50%;
    display: flex;
    flex-flow: column nowrap;
    justify-content: center;
    align-items: center;
`;

export const Welcome = styled.p`
    @import url('https://fonts.googleapis.com/css2?family=Roboto:wght@500;700&display=swap');
    width: 120px;
    font-family: 'Roboto', sans-serif;
    font-size: 16px;
    font-weight: bold;
    text-align: center;
    color: #383743;
    margin-bottom: 0;
`

export const Description = styled.p`
    @import url('https://fonts.googleapis.com/css2?family=Roboto:wght@500;700&display=swap');
    width: 200px;
    font-family: 'Roboto', sans-serif;
    font-size: 11px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.44;
    letter-spacing: -0.13px;
    text-align: center;
    color: #383743;
`



export const MailInput = styled.input`
    @import url('https://fonts.googleapis.com/css2?family=Roboto:wght@500;700&display=swap');
    background: transparent;
    background-image: url(${mailIcon});
    background-repeat: no-repeat;
    background-size: 30px;
    width: 210px;
    font-family: 'Roboto', sans-serif;
    font-size: 11px;
    padding: 10px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    border: none;
    border-bottom: 1px solid #888785;
    color: #888785;
    padding-left: 30px;


`

export const PasswordInput = styled.input`
    @import url('https://fonts.googleapis.com/css2?family=Roboto:wght@500;700&display=swap');
    background: transparent;
    background-image: url(${passwordIcon});
    background-repeat: no-repeat;
    background-size: 30px;
    width: 210px;
    font-family: 'Roboto', sans-serif;
    font-size: 11px;
    padding: 10px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    border: none;
    border-bottom: 1px solid #888785;
    color: #888785;
    margin-top: 15%;
    padding-left: 30px;

`

export const SubmitButton = styled.input`
    @import url('https://fonts.googleapis.com/css2?family=Roboto:wght@500;700&display=swap');
    width: 180px;
    height: 26.4px;
    border-radius: 1.8px;
    font-family: 'Roboto', sans-serif;
    background-color: #57bbbc;
    font-size: 11px;
    font-weight: 700;
    border: none;
    color: #ffffff;
    margin-top: 20%;
`

export const Form = styled.form`
    display: flex;
    flex-flow: column nowrap;
    justify-content: center;
    align-items: center;
`; 