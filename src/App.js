import React from "react";
import Login from "./containers/Login";
import EnterprisesFeed from "./containers/EnterprisesFeed";
import Enterprise from "./containers/Enterprise";
import { Background } from "./AppStyles";
import { BrowserRouter, Switch, Route } from "react-router-dom";

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Background>
          <Route 
            path="/enterprises" 
            exact
            component={EnterprisesFeed} 
          />
          <Route
            path="/enterprises/:id"
            component={Enterprise}
          />
          <Route 
            path="/" 
            exact 
            component={Login} 
          />
        </Background>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
