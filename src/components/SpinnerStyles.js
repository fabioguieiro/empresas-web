import styled from 'styled-components'

export const Spinner = styled.div`
    position: absolute;
    top: 45%;
    left: 40%;
    z-index: 100;
    border: 8px solid rgba(0, 0, 0, 0.1);
    border-left-color: #57bbbc;
    border-radius: 50%;
    width: 60px;
    height: 60px;
    animation: spin 1s linear infinite;
    @keyframes spin {
    to {
        transform: rotate(360deg);
    }
    }
    @media(min-width: 500px){
        top: 45%;
        left: 48.5%;
    }
`